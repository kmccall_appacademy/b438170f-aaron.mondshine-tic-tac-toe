class HumanPlayer
  attr_accessor :name

  def initialize(human)
    @name = human
  end

  def get_move
    puts "Where would you like to go?"
    @pos = gets.chomp.split(", ").map.each(&:to_i)
  end

  def display(board)
    board.grid.each do |rows|
      puts rows
    end
  end
end
