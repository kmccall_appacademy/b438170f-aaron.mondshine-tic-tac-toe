class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name

  end

  def display(board1)
    @board = board1

  end

  def get_move
    board.h_z_v.each do |key, a1|
      if !arr_looper(a1).nil?
        case key
        when :horizontal
          return arr_looper(a1)
        when :vertical
          return [arr_looper(a1)[1], arr_looper(a1)[0]]
        when :diag_r
          return [arr_looper(a1)[1], arr_looper(a1)[1]]
        else
          return [2 - arr_looper(a1)[1], 2 - arr_looper(a1)[1]]
        end
      end
    end
    random_move
  end

  def arr_looper(a1)
    a1.each_with_index do |a2, idx1|
      two = a2.select { |x| x == @mark }.length
      a2.each_with_index do |el, idx2|
        return [idx1, idx2] if el == nil && two == 2
      end
    end
    nil
  end

  def random_move
    possible_moves = []
    board.grid.each_index do |r|
      board.grid.each_index do |c|
        possible_moves << [r, c] if board.empty?([r, c])
      end
    end
    possible_moves.shuffle[0]
  end

end
