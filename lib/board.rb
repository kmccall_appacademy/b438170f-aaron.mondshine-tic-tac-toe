class Board
  attr_accessor :grid, :place_marks

  def initialize(grid = Array.new(3) { [nil, nil, nil] })
    @grid = grid
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]] == nil ? true : false
  end

  def winner
    h_z_v.values.each do |a1|
      a1.each do |a2|
        return :X if a2.all? { |el| el == :X }
        return :O if a2.all? { |el| el == :O }
      end
    end
    nil
  end

  def h_z_v
    diag_r, diag_l, horizontal, vertical = [], [], [], []
    @grid.each_index do |i1|
      in_threes_v, in_threes_h = [], []
      @grid.each_index do |i2|
        in_threes_h << @grid[i1][i2]
        in_threes_v << @grid[i2][i1]
      end
      horizontal << in_threes_h
      vertical << in_threes_v
      diag_r << in_threes_v[i1]
      diag_l << in_threes_v[2 - i1]
    end
    { diag_r: [diag_r], diag_l: [diag_l], vertical: vertical, horizontal: horizontal }
  end

  def over?
    return true if !winner.nil? || @grid.flatten.none? { |x| x == nil }
    return false if @grid.flatten.any? { |x| x == nil }
  end

end
